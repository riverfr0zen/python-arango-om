This is a rough list of things to do before initial release (0.1.0)

# Core

# TODO
* General
    - configure method to inject OM settings including & beyond db settings
    - Migration tools 
    - Handling of all ~~(except special ones, e.g. that return a cursor)~~ CRUD methods proxied to Collection by Model. For reads, load results as Model objects. For writes, allow passing in data as Model instances.
        + ResultSet
        + GraphResultSet
            * __iter__ methods 'expandDeeply' 'expandWidely' (may need to be implemented using some RS object property like rs.iter_method)
        + Testing of above handling
    - Keep count of items in each collection via registry (and update reg on create, delete)
* Querying
    - Support for AND or OR, and other things not provided by API (See https://github.com/joowani/python-arango/issues/40)
* Graphs
    - mapping for graph objects (necessary?)
* Access Control
    - Models can define types of permissions available on them (and perhaps default roles or groups that can access them) 
    - Re-evaluate if this is necessary
* Testing
    - Utility script to run tests
    - Utility script to configure db and run integration tests
    - Switch to pytest or nose2 (?)
    - Test coverage
* Look into Dash for frontend
    - https://medium.com/@plotlygraphs/introducing-dash-5ecf7191b503
* Schema objects layer
    - Explore making package agnostic of which objects package to use (i.e. make Schematics interchangeable with, for e.g. marshmallow)
    - SimpleJSON objects package, for ppl who think object layers are too much (Model(field1, field2...) just returns a json object loaded with the data passed in)
    - Django models as object layer? amusing idea.

## DONE

* General
    - ~~fix having to invoke `Model._initialize_collection()` for each test (after the database is cleared)~~
    - ~~Profiling~~ (using snakeviz)
    - ~~Schema + validation + ...~~
    - ~~schema property in classes needs to be appendable in subclasses. see:
        + (https://stackoverflow.com/questions/38987/how-to-merge-two-dictionaries-in-a-single-expression)
        + https://stackoverflow.com/questions/10119233/python-appending-to-class-level-lists-in-derived-class-definitions
        + @NOTE: The solution may come inherentely by redesigning fields so they are declared as properties instead of items in the 'schema' property, or by adopting an existing packaging for schemas (e.g. Schematics)~~
    - ~~W/ Schematics: Need a way to override default settings (and other settings) for schema fields in Model subclasses without re-declaring the whole field~~ (if necessary use copy as in tests/test_models.py)
    - ~~Models aware of 'expected edge types' e.g. Apple knows it might have a `is_child_of` edge~~ (from_edges, to_edges properties on VertexModel)
    - ~~Get rid of settings.py requirement in core/__init__.py. 
        + Explore getting rid of bind_datasource and moving that functionality to ModelMeta. You can pass in DB params and so on to the metaclass, see [link](https://stackoverflow.com/questions/13762231/python-metaclass-arguments)
        + The above will let you just pass in db details easily, and makes class initialization more direct~~
* Graphs
    - ~~VertexModel objects should have a convenience 'traverse' method (i.e. traverse from this vertex)~~
* Testing
    - ~~Make GDBExperimentsTestCase use ConnectionPool in 'unintegrated' mode, to make usable in non-integration tests~~
    - ~~Make new GDBExperimentsIntegratedTestCase that connects to real mode ConnectionPool~~
    - ~~rename to PAOTestCase and PAOIntegrationTestCase~~
