import unittest
from pao.tests.pao_test_case import PAOTestCase, TEST_DB_NAME
from pao import VertexModel, EdgeModel
from schematics.types import StringType, IntType


class Creature(VertexModel):
    class Schema(VertexModel.Schema):
        name = StringType(required=True, default='n/a')
        heads = IntType(required=True, default=1)
        torsos = IntType(required=True, default=1)
        arms = IntType(required=True, default=2)
        legs = IntType(required=True, default=2)


class Nest(VertexModel):
    class Schema(VertexModel.Schema):
        name = StringType(required=True, default='n/a')
        health = IntType(required=True, default=0)


class LivedIn(EdgeModel):
    from_models = ['Creature']
    to_models = ['Nest']

    class Schema(EdgeModel.Schema):
        start = StringType(required=True, default='')
        until = StringType(required=True, default='')
        status = StringType(default='Normal')


class TunnelsTo(EdgeModel):
    from_models = ['Nest']
    to_models = ['Nest']

    class Schema(EdgeModel.Schema):
        length = IntType(default=0)


class DifferentGraphExampleNode(VertexModel,
                                graph_name="somegraph",
                                collection_name="diff_example_nodes"):
    class Schema(VertexModel.Schema):
        length = IntType(default=0)


class DifferentGraphExampleEdge(EdgeModel,
                                graph_name="somegraph",
                                collection_name="diff_example_edges"):
    from_models = ['DifferentGraphExampleNode']
    to_models = ['DifferentGraphExampleNode']

    class Schema(EdgeModel.Schema):
        # @TODO make these datetime types
        length = IntType(default=0)


class TestGraphModels(PAOTestCase):
    def test_model_meta_prepare(self):
        self.assertEqual(Creature.db_name, TEST_DB_NAME)
        self.assertEqual(Creature.graph_name, 'default')

        self.assertEqual(Nest.db_name, TEST_DB_NAME)
        self.assertEqual(Nest.collection_name, 'nests')
        self.assertEqual(Nest.graph_name, 'default')

        self.assertEqual(LivedIn.db_name, TEST_DB_NAME)
        self.assertEqual(LivedIn.collection_name, 'lived_ins')
        self.assertEqual(LivedIn.graph_name, 'default')

        self.assertEqual(TunnelsTo.db_name, TEST_DB_NAME)
        self.assertEqual(TunnelsTo.collection_name, 'tunnels_tos')
        self.assertEqual(TunnelsTo.graph_name, 'default')

        self.assertEqual(DifferentGraphExampleNode.db_name,
                         TEST_DB_NAME)
        self.assertEqual(DifferentGraphExampleNode.collection_name,
                         'diff_example_nodes')
        self.assertEqual(DifferentGraphExampleNode.graph_name, 'somegraph')

        self.assertEqual(DifferentGraphExampleEdge.db_name,
                         TEST_DB_NAME)
        self.assertEqual(DifferentGraphExampleEdge.collection_name,
                         'diff_example_edges')
        self.assertEqual(DifferentGraphExampleEdge.graph_name, 'somegraph')

    def test_model_instantiation(self):
        l1 = LivedIn(**{"_id": "lived_in/000", "_key": "xyz",
                        "_from": "creatures/100", "_to": "nests/100",
                        "start": '1978', "until": "1980"})
        self.assertIsInstance(l1, LivedIn)
        self.assertEqual(l1._id, 'lived_in/000')
        self.assertEqual(l1._key, 'xyz')
        self.assertEqual(l1._from, 'creatures/100')
        self.assertEqual(l1._to, 'nests/100')
        self.assertEqual(l1.start, '1978')
        self.assertEqual(l1.until, '1980')
        self.assertEqual(l1.status, 'Normal')

    def test_edge_model_attributes(self):
        self.assertEqual(len(LivedIn.from_collections()), 1)
        self.assertTrue('creatures' in LivedIn.from_collections())
        self.assertEqual(len(LivedIn.to_collections()), 1)
        self.assertTrue('nests' in LivedIn.to_collections())
        self.assertEqual(len(TunnelsTo.from_collections()), 1)
        self.assertTrue('nests' in TunnelsTo.from_collections())
        self.assertEqual(len(TunnelsTo.to_collections()), 1)
        self.assertTrue('nests' in TunnelsTo.to_collections())

    def test_to_and_from_edges_properties(self):
        c1 = Creature(name="Oook")
        # logging.debug(c1.to_edges)
        self.assertIsInstance(c1.to_edges, list)
        self.assertEquals(len(c1.to_edges), 1)
        self.assertEquals(c1.to_edges[0], 'LivedIn')
        self.assertIsInstance(c1.from_edges, list)
        self.assertEquals(len(c1.from_edges), 0)

        n1 = Nest(name="Oook")
        # logging.debug(n1.from_edges)
        self.assertIsInstance(n1.to_edges, list)
        self.assertEquals(len(n1.to_edges), 1)
        self.assertEquals(n1.to_edges[0], 'TunnelsTo')
        self.assertIsInstance(n1.from_edges, list)
        self.assertEquals(len(n1.from_edges), 2)
        self.assertTrue('TunnelsTo' in n1.from_edges)
        self.assertTrue('LivedIn' in n1.from_edges)


if __name__ == '__main__':
    unittest.main()
