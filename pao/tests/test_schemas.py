import unittest
import logging
from arango.collections.standard import Collection
from pao.tests.pao_test_case import PAOTestCase
from pao import Model, ModelMeta
from schematics.models import Model as SchematicsModel
from schematics.types import StringType, IntType, BooleanType


class Fruit(Model):
    class Schema(Model.Schema):
        color = StringType(required=True, default='n/a')
        taste = StringType(required=True, default='n/a')
        price = IntType(required=True, default=0)


class Apple(Fruit):
    class Schema(Fruit.Schema):
        color = StringType(required=True, default='red')
        taste = StringType(required=True, default='sweet')
        is_poisoned = BooleanType(required=False, default=False)


class Mi1(Model):
    class Schema(Model.Schema):
        a = StringType(required=True, default='Mi1.a')
        b = IntType(required=True, default=10)

    def z(self):
        return 'Mi1.z()'


class Mi2(Model):
    class Schema(Model.Schema):
        a = StringType(required=True, default='Mi2.a')
        c = IntType(required=True, default=20)
        d = StringType(required=True, default='Mi2.d')

    def y(self):
        return 'Mi2.y()'

    def z(self):
        return 'Mi2.z()'


class Mi12(Mi1, Mi2, metaclass=ModelMeta, collection_name='mis'):
    new_schema_attrs = {
        'e': StringType(required=True, default='Mi12.e')
    }


class TestModelSchemas(PAOTestCase):
    def test_schema_inheritance(self):
        assert(issubclass(Model.Schema, SchematicsModel))
        assert(issubclass(Fruit.Schema, SchematicsModel))
        self.assertIsInstance(Fruit.Schema._key, StringType)
        self.assertIsInstance(Fruit.Schema._id, StringType)
        self.assertIsInstance(Apple.Schema.is_poisoned, BooleanType)

        self.assertNotEqual(Fruit.Schema.color._default,
                            Apple.Schema.color._default)
        self.assertNotEqual(Fruit.Schema.taste._default,
                            Apple.Schema.taste._default)
        self.assertEqual(Fruit.Schema.price._default,
                         Apple.Schema.price._default)

    def test_multiple_inheritance(self):
        # assert(issubclass(Mi12.Schema, Model.Schema))
        assert(hasattr(Mi12.Schema, 'a'))
        assert(hasattr(Mi12.Schema, 'b'))
        assert(hasattr(Mi12.Schema, 'c'))
        assert(hasattr(Mi12.Schema, 'd'))
        assert(hasattr(Mi12.Schema, 'e'))
        assert(hasattr(Mi12, 'y'))
        assert(hasattr(Mi12, 'z'))

        mixed = Mi12(c=30)
        self.assertEqual(mixed.a, 'Mi1.a')
        self.assertEqual(mixed.b, 10)
        self.assertEqual(mixed.c, 30)
        self.assertEqual(mixed.d, 'Mi2.d')
        self.assertEqual(mixed.e, 'Mi12.e')
        self.assertEqual(mixed.z(), 'Mi1.z()')
        self.assertEqual(mixed.y(), 'Mi2.y()')

if __name__ == '__main__':
    unittest.main()
