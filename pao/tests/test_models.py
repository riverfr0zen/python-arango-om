import unittest
import logging
from copy import copy
from arango.database import Database
from arango.collections.standard import Collection
from pao import Model
from pao.tests.pao_test_case import PAOTestCase, TEST_DB_NAME1, TEST_DB_NAME
from schematics.types import StringType, IntType, BooleanType


class FruitSchema(Model.Schema):
    color = StringType(required=True, default='n/a')
    taste = StringType(required=True, default='n/a')
    price = IntType(required=True, default=0)


class Apple(Model):
    class Schema(FruitSchema):
        color = copy(FruitSchema.color)
        color._default = 'red'
        taste = StringType(required=True, default='sweet')
        is_poisoned = BooleanType(required=False, default=False)


class CustardApple(Model, db_name=TEST_DB_NAME1):
    class Schema(FruitSchema):
        color = StringType(required=True, default='yellow')
        price = IntType(required=True, default=50)


class Orange(Model):
    class Schema(FruitSchema):
        color = StringType(required=True, default='orange')
        price = IntType(required=True, default=10)
        taste = StringType(required=True, default='sour')


class WaterMelon(Model, collection_name='watermellies'):
    class Schema(FruitSchema):
        color = StringType(required=True, default='green')
        price = IntType(required=True, default=50)
        size = StringType(required=True, default='large')


class TestModels(PAOTestCase):
    def test_model_meta_prepare(self):
        self.assertEqual(Apple.db_name, TEST_DB_NAME)
        self.assertEquals(Apple.collection_name, 'apples')

        self.assertEqual(CustardApple.db_name, TEST_DB_NAME1)
        self.assertEquals(CustardApple.collection_name, 'custard_apples')

        self.assertEqual(Orange.db_name, TEST_DB_NAME)
        self.assertEquals(Orange.collection_name, 'oranges')

        self.assertEqual(WaterMelon.db_name, TEST_DB_NAME)
        self.assertEquals(WaterMelon.collection_name, 'watermellies')


    def test_model_instantiation(self):
        a1 = Apple(color='pink', taste='unflavorful')
        self.assertIsInstance(a1, Apple)
        self.assertEqual(a1.Schema, Apple.Schema)

        o1 = Orange(color='blue', taste='tangy', price=60)
        self.assertIsInstance(o1, Orange)

        w1 = WaterMelon(taste='sour')
        self.assertIsInstance(w1, WaterMelon)

    def test_set_doc_properties(self):
        a1 = Apple()
        a1._set_doc_properties(color='blue', taste='amazing')
        self.assertIsInstance(a1._fields, Apple.Schema)
        self.assertEqual(a1._fields.color, 'blue')
        self.assertEqual(a1._fields.taste, 'amazing')
        self.assertEqual(a1._fields.price, 0)
        self.assertEqual(a1._fields.is_poisoned, False)

        self.assertEqual(a1.color, 'blue')
        self.assertEqual(a1.taste, 'amazing')
        self.assertEqual(a1.price, 0)
        self.assertEqual(a1.is_poisoned, False)

        o1 = Orange(color='blue', taste='tangy', price=60)
        self.assertIsInstance(o1, Orange)
        self.assertEqual(o1.color, 'blue')
        self.assertEqual(o1.taste, 'tangy')
        self.assertEqual(o1.price, 60)

        w1 = WaterMelon(taste='putrid')
        self.assertIsInstance(w1._fields, WaterMelon.Schema)
        self.assertEqual(w1._fields.color, 'green')
        self.assertEqual(w1._fields.taste, 'putrid')
        self.assertEqual(w1._fields.price, 50)
        self.assertEqual(w1._fields.size, 'large')

        self.assertEqual(w1.color, 'green')
        self.assertEqual(w1.taste, 'putrid')
        self.assertEqual(w1.price, 50)
        self.assertEqual(w1.size, 'large')


if __name__ == '__main__':
    unittest.main()
