import unittest
from arango import ArangoClient
from pao import ConnectionPool
from pao.tests.utils import resolve_integration
try:
    import settings
except:
    settings = resolve_integration()


TEST_DB_NAME = "%s_test" % settings.DB_NAME
TEST_DB_NAME1 = "%s_test_1" % settings.DB_NAME
ConnectionPool.connect(settings.DB_CLI, TEST_DB_NAME)


class PAOIntegratedTestCase(unittest.TestCase):
    def setUp(self):
        self.dbcli = ArangoClient(**settings.DB_CLI)
        self.db = self.dbcli.database(TEST_DB_NAME)
        if len(ConnectionPool.dbs) < 1:
            ConnectionPool.connect(settings.DB_CLI, TEST_DB_NAME)

    def tearDown(self):
        """ Clear data generated during tests from collections & graphs """
        for collection_def in self.db.collections():
            # print(collection_def)
            if not collection_def['system']:
                collection = self.db.collection(collection_def['name'])
                collection.delete_many([document for document
                                        in collection.all()])
        # In the future, we may need to do something w/ graphs too
        # for graph in self.db.graphs():
        #     self.db.delete_graph(graph['name'])
        #     self.db.create_graph(graph['name'])
        ConnectionPool.reset()
        pass
