import logging
import unittest
from pao import InterceptingProxyMeta


class ProxiedItem:
    a = 'a'
    b = 'b'
    c = 'c'

    def z(self):
        return 'z()'

    @classmethod
    def y(cls):
        return 'y()'

    @classmethod
    def x(cls):
        return 'x()'

    @classmethod
    def w(cls):
        return 'w()'


class UserClass1(metaclass=InterceptingProxyMeta):
    proxied = ProxiedItem()
    c = 'UC c'

    @classmethod
    def x(cls):
        return 'UC x()'


class UserClass2(metaclass=InterceptingProxyMeta):
    proxied = ProxiedItem()
    proxy_method_handlers = {
        "z_and_y_handler": ('z', 'y',),
        "x_and_w_handler": ('x', 'w',)
    }
    c = 'UC c'

    @classmethod
    def x(cls):
        return 'UC x()'

    @classmethod
    def z_and_y_handler(cls, func, *args, **kwargs):
        return {'result': "zy__%s__zy" % func(*args, **kwargs)}

    @classmethod
    def x_and_w_handler(cls, func, *args, **kwargs):
        return {'result': "xw__%s__xw" % func(*args, **kwargs)}


class TestInterceptingProxyMeta(unittest.TestCase):
    def test_interception(self):
        self.assertEqual(UserClass1.a, 'a')
        self.assertEqual(UserClass1.b, 'b')
        self.assertEqual(UserClass1.c, 'UC c')
        self.assertEqual(UserClass1.z(), 'z()')
        self.assertEqual(UserClass1.y(), 'y()')
        self.assertEqual(UserClass1.x(), 'UC x()')
        uc1 = UserClass1()
        # Following 2 should not work b/c __getattr__ in InterceptingProxyMeta
        # only operates on classes, not objects
        with self.assertRaises(AttributeError):
            uc1.z()
        with self.assertRaises(AttributeError):
            uc1.y()
        # This is ok b/c x() is a classmethod of UserClass1 and uc1 is an
        # instance of it
        self.assertEqual(uc1.x(), 'UC x()')
        self.assertEqual(uc1.c, 'UC c')

        self.assertIs(UserClass1.a, ProxiedItem.a)
        self.assertIs(UserClass1.b, ProxiedItem.b)
        self.assertNotEqual(UserClass1.c, ProxiedItem.c)
        self.assertNotEqual(UserClass1.z, ProxiedItem.z)
        self.assertEqual(UserClass1.z(), UserClass1.proxied.z())
        self.assertEqual(UserClass1.y(), ProxiedItem.y())
        self.assertNotEqual(UserClass1.x(), ProxiedItem.x())

    def test_proxy_method_handlers(self):
        self.assertIsInstance(UserClass2.z(), dict)
        self.assertEqual(UserClass2.z()['result'], 'zy__z()__zy')

        self.assertIsInstance(UserClass2.y(), dict)
        self.assertEqual(UserClass2.y()['result'], 'zy__y()__zy')

        self.assertFalse(isinstance(UserClass2.x(), dict))
        self.assertEqual(UserClass2.x(), 'UC x()')

        self.assertIsInstance(UserClass2.w(), dict)
        self.assertEqual(UserClass2.w()['result'], 'xw__w()__xw')
