import unittest
from arango import ArangoClient
from pao.tests.pao_integrated_test_case import PAOIntegratedTestCase
from pao.tests.utils import resolve_integration
try:
    import settings
except:
    settings = resolve_integration()


class TestDb(PAOIntegratedTestCase):

    def test_dbcli(self):
        self.assertIsInstance(self.dbcli, ArangoClient)
        self.assertEqual(self.dbcli.protocol, settings.DB_CLI['protocol'])
        self.assertEqual(self.dbcli.host, settings.DB_CLI['host'])
        self.assertEqual(self.dbcli.username, settings.DB_CLI['username'])

    def test_db(self):
        self.assertEqual(self.db.name, settings.DB_NAME + '_test')


if __name__ == '__main__':
    unittest.main()
