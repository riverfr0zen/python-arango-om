import unittest
import logging
from arango.graph import Graph
from arango.collections.vertex import VertexCollection
from arango.collections.edge import EdgeCollection
from pao.tests.pao_integrated_test_case import PAOIntegratedTestCase
from pao.tests.pao_integrated_test_case import TEST_DB_NAME
from pao import VertexModel, EdgeModel
from schematics.types import StringType, IntType


class Creature(VertexModel):
    class Schema(VertexModel.Schema):
        name = StringType(required=True, default='n/a')
        heads = IntType(required=True, default=1)
        torsos = IntType(required=True, default=1)
        arms = IntType(required=True, default=2)
        legs = IntType(required=True, default=2)


class Nest(VertexModel):
    class Schema(VertexModel.Schema):
        name = StringType(required=True, default='n/a')
        health = IntType(required=True, default=0)


class LivedIn(EdgeModel):
    from_models = ['Creature']
    to_models = ['Nest']

    class Schema(EdgeModel.Schema):
        # @TODO make these datetime types
        start = StringType(required=True, default='')
        until = StringType(required=True, default='')
        status = StringType(default='Normal')


class TunnelsTo(EdgeModel):
    from_models = ['Nest']
    to_models = ['Nest']

    class Schema(EdgeModel.Schema):
        # @TODO make these datetime types
        length = IntType(default=0)


class DifferentGraphExampleNode(VertexModel,
                                graph_name="somegraph",
                                collection_name="diff_example_node"):
    class Schema(VertexModel.Schema):
        # @TODO make these datetime types
        length = IntType(default=0)


class DifferentGraphExampleEdge(EdgeModel,
                                graph_name="somegraph",
                                collection_name="diff_example_edge"):
    from_models = ['DifferentGraphExampleNode']
    to_models = ['DifferentGraphExampleNode']

    class Schema(EdgeModel.Schema):
        # @TODO make these datetime types
        length = IntType(default=0)


class TestGraphModels(PAOIntegratedTestCase):
    def test_get_graph_object(self):
        self.assertIsInstance(Creature.graph, Graph)
        self.assertEqual(Creature.graph.properties()['name'], 'default')

        self.assertIsInstance(Nest.graph, Graph)
        self.assertEqual(Nest.graph.properties()['name'], 'default')

        self.assertIsInstance(LivedIn.graph, Graph)
        self.assertEqual(LivedIn.graph.properties()['name'], 'default')

        self.assertIsInstance(TunnelsTo.graph, Graph)
        self.assertEqual(TunnelsTo.graph.properties()['name'], 'default')

        self.assertIsInstance(DifferentGraphExampleNode.graph, Graph)
        self.assertEqual(DifferentGraphExampleNode.graph.properties()['name'],
                         'somegraph')

        self.assertIsInstance(DifferentGraphExampleEdge.graph, Graph)
        self.assertEqual(DifferentGraphExampleEdge.graph.properties()['name'],
                         'somegraph')

    def test_get_collection_object(self):
        self.assertEqual(Creature.db_name, TEST_DB_NAME)
        self.assertNotEqual(Creature.collection, None)
        self.assertIsInstance(Creature.collection, VertexCollection)

        self.assertEqual(Nest.db_name, TEST_DB_NAME)
        self.assertNotEqual(Nest.collection, None)
        self.assertIsInstance(Nest.collection, VertexCollection)

        self.assertEqual(LivedIn.db_name, TEST_DB_NAME)
        self.assertNotEqual(LivedIn.collection, None)
        self.assertIsInstance(LivedIn.collection, EdgeCollection)

        self.assertEqual(TunnelsTo.db_name, TEST_DB_NAME)
        self.assertNotEqual(TunnelsTo.collection, None)
        self.assertIsInstance(TunnelsTo.collection, EdgeCollection)

        self.assertEqual(DifferentGraphExampleNode.db_name,
                         TEST_DB_NAME)
        self.assertNotEqual(DifferentGraphExampleNode.collection, None)
        self.assertIsInstance(DifferentGraphExampleNode.collection,
                              VertexCollection)

        self.assertEqual(DifferentGraphExampleEdge.db_name,
                         TEST_DB_NAME)
        self.assertNotEqual(DifferentGraphExampleEdge.collection, None)
        self.assertIsInstance(DifferentGraphExampleEdge.collection,
                              EdgeCollection)

    def test_model_instantiation(self):
        l1 = LivedIn(**{"_id": "lived_in/000", "_key": "xyz",
                        "_from": "creatures/100", "_to": "nests/100",
                        "start": '1978', "until": "1980"})
        self.assertIsInstance(l1, LivedIn)
        self.assertEqual(l1._id, 'lived_in/000')
        self.assertEqual(l1._key, 'xyz')
        self.assertEqual(l1._from, 'creatures/100')
        self.assertEqual(l1._to, 'nests/100')
        self.assertEqual(l1.start, '1978')
        self.assertEqual(l1.until, '1980')
        self.assertEqual(l1.status, 'Normal')

    def test_insert(self):
        c1 = Creature.insert({"name": "c1"})
        c2 = Creature.insert({"name": "c2"})
        c3 = Creature.insert({"name": "c3"})
        self.assertNotEqual(c1._key, None)
        self.assertNotEqual(c1._id, None)
        self.assertNotEqual(c2._key, None)
        self.assertNotEqual(c2._id, None)
        self.assertNotEqual(c3._key, None)
        self.assertNotEqual(c3._id, None)

        n1 = Nest.insert({"name": "n1"})
        n2 = Nest.insert({"name": "n2"})
        n3 = Nest.insert({"name": "n3"})
        self.assertNotEqual(n1._key, None)
        self.assertNotEqual(n1._id, None)
        self.assertNotEqual(n2._key, None)
        self.assertNotEqual(n2._id, None)
        self.assertNotEqual(n3._key, None)
        self.assertNotEqual(n3._id, None)

        l1 = LivedIn.insert({"_from": c1._id, "_to": n1._id,
                             "start": "1950", "until": "1978"})
        l2 = LivedIn.insert({"_from": c1._id, "_to": n2._id,
                             "start": "1978", "until": "1980"})
        self.assertNotEqual(l1._key, None)
        self.assertNotEqual(l1._id, None)
        self.assertEqual(l1._from, c1._id)
        self.assertEqual(l1._to, n1._id)
        self.assertNotEqual(l2._key, None)
        self.assertNotEqual(l2._id, None)
        self.assertEqual(l2._from, c1._id)
        self.assertEqual(l2._to, n2._id)

        l3 = LivedIn.insert({"_from": c2._id, "_to": n1._id,
                             "start": "1950", "until": "1960"})
        l4 = LivedIn.insert({"_from": c2._id, "_to": n3._id,
                             "start": "1960", "until": "1985"})
        self.assertNotEqual(l3._key, None)
        self.assertNotEqual(l3._id, None)
        self.assertEqual(l3._from, c2._id)
        self.assertEqual(l3._to, n1._id)
        self.assertNotEqual(l4._key, None)
        self.assertNotEqual(l4._id, None)
        self.assertEqual(l4._from, c2._id)
        self.assertEqual(l4._to, n3._id)

        t1 = TunnelsTo.insert({"_from": n1._id, "_to": n3._id, "length": 2})
        self.assertNotEqual(t1._key, None)
        self.assertNotEqual(t1._id, None)
        self.assertEqual(t1._from, n1._id)
        self.assertEqual(t1._to, n3._id)

    def test_class_attributes(self):
        c1 = Creature.insert({"name": "c1"})
        c2 = Creature.insert({"name": "c2"})
        self.assertEqual(c1.graph, Creature.graph)
        self.assertEqual(c2.graph, Creature.graph)

    def test_edge_model_attributes(self):
        self.assertEqual(len(LivedIn.from_collections()), 1)
        self.assertTrue('creatures' in LivedIn.from_collections())
        self.assertEqual(len(LivedIn.to_collections()), 1)
        self.assertTrue('nests' in LivedIn.to_collections())
        self.assertEqual(len(TunnelsTo.from_collections()), 1)
        self.assertTrue('nests' in TunnelsTo.from_collections())
        self.assertEqual(len(TunnelsTo.to_collections()), 1)
        self.assertTrue('nests' in TunnelsTo.to_collections())

    def test_create_path_to(self):
        c1 = Creature.insert({"name": "c1"})
        n1 = Nest.insert({"name": "n1"})
        n2 = Nest.insert({"name": "n2"})
        l1 = c1.create_path_to(n1, LivedIn, {"start": "1999"})
        t1 = n1.create_path_to(n2, TunnelsTo, {"length": "2"})

        self.assertIsInstance(l1, LivedIn)
        self.assertNotEqual(l1._id, None)
        self.assertEqual(LivedIn.all().count(), 1)
        self.assertEqual(l1._from, c1._id)
        self.assertEqual(l1._to, n1._id)

        self.assertIsInstance(t1, TunnelsTo)
        self.assertNotEqual(t1._id, None)
        self.assertEqual(TunnelsTo.all().count(), 1)
        self.assertEqual(t1._from, n1._id)
        self.assertEqual(t1._to, n2._id)

    def test_traverse_from_model(self):
        c1 = Creature.insert({"name": "c1"})
        c2 = Creature.insert({"name": "c2"})
        c3 = Creature.insert({"name": "c3"})
        n1 = Nest.insert({"name": "n1"})
        n2 = Nest.insert({"name": "n2"})
        n3 = Nest.insert({"name": "n3"})
        l1 = c1.create_path_to(n1, LivedIn, {"start": "1950", "until": "1978"})
        l2 = c1.create_path_to(n2, LivedIn, {"start": "1978", "until": "1980"})
        l3 = c2.create_path_to(n1, LivedIn, {"start": "1950", "until": "1960"})
        l4 = c2.create_path_to(n3, LivedIn, {"start": "1960", "until": "1985"})
        l5 = c3.create_path_to(n1, LivedIn, {"start": "1920", "until": "2000"})
        l6 = c3.create_path_to(n3, LivedIn, {"start": "2000", "until": "2005"})
        l7 = c3.create_path_to(n2, LivedIn, {"start": "2000"})
        t1 = n1.create_path_to(n3, TunnelsTo, {"length": 2})

        self.assertEqual(LivedIn.all().count(), 7)
        self.assertIsInstance(l1, EdgeModel)
        self.assertIsInstance(l2, EdgeModel)
        self.assertIsInstance(l3, EdgeModel)
        self.assertIsInstance(l4, EdgeModel)
        self.assertIsInstance(l5, EdgeModel)
        self.assertIsInstance(l6, EdgeModel)
        self.assertIsInstance(l7, EdgeModel)

        self.assertEqual(TunnelsTo.all().count(), 1)
        self.assertIsInstance(t1, EdgeModel)

        tresults = c1.traverse(direction='outbound')
        # logging.debug(pprint.pprint(tresults))
        self.assertEqual(len(tresults['vertices']), 4)
        self.assertEqual(len([vertex for vertex in tresults['vertices']
                              if vertex['_id'] == c1._id]),
                         1)
        self.assertEqual(len([vertex for vertex in tresults['vertices']
                              if vertex['_id'] == n1._id]),
                         1)
        self.assertEqual(len([vertex for vertex in tresults['vertices']
                              if vertex['_id'] == n2._id]),
                         1)
        self.assertEqual(len([vertex for vertex in tresults['vertices']
                              if vertex['_id'] == n3._id]),
                         1)

        tresults = n2.traverse(direction='inbound', max_depth=1)
        # logging.debug(pprint.pprint(tresults))
        self.assertEqual(len(tresults['vertices']), 3)

    def test_to_and_from_edges_properties(self):
        c1 = Creature(name="Oook")
        # logging.debug(c1.to_edges)
        self.assertIsInstance(c1.to_edges, list)
        self.assertEquals(len(c1.to_edges), 1)
        self.assertEquals(c1.to_edges[0], 'LivedIn')
        self.assertIsInstance(c1.from_edges, list)
        self.assertEquals(len(c1.from_edges), 0)

        n1 = Nest(name="Oook")
        # logging.debug(n1.from_edges)
        self.assertIsInstance(n1.to_edges, list)
        self.assertEquals(len(n1.to_edges), 1)
        self.assertEquals(n1.to_edges[0], 'TunnelsTo')
        self.assertIsInstance(n1.from_edges, list)
        self.assertEquals(len(n1.from_edges), 2)
        self.assertTrue('TunnelsTo' in n1.from_edges)
        self.assertTrue('LivedIn' in n1.from_edges)


if __name__ == '__main__':
    unittest.main()
