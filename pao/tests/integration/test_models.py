import unittest
import logging
from copy import copy
from arango.database import Database
from arango.collections.standard import Collection
from pao import Model
from pao.tests.pao_integrated_test_case import PAOIntegratedTestCase
from pao.tests.pao_integrated_test_case import TEST_DB_NAME1, TEST_DB_NAME
from schematics.types import StringType, IntType, BooleanType



class FruitSchema(Model.Schema):
    color = StringType(required=True, default='n/a')
    taste = StringType(required=True, default='n/a')
    price = IntType(required=True, default=0)


class Apple(Model):
    class Schema(FruitSchema):
        color = copy(FruitSchema.color)
        color._default = 'red'
        taste = StringType(required=True, default='sweet')
        is_poisoned = BooleanType(required=False, default=False)


class CustardApple(Model, db_name=TEST_DB_NAME1):
    class Schema(FruitSchema):
        color = StringType(required=True, default='yellow')
        price = IntType(required=True, default=50)


class Orange(Model):
    class Schema(FruitSchema):
        color = StringType(required=True, default='orange')
        price = IntType(required=True, default=10)


class WaterMelon(Model, collection_name='watermellies'):
    class Schema(FruitSchema):
        color = StringType(required=True, default='yellow')
        price = IntType(required=True, default=50)


class TestModels(PAOIntegratedTestCase):
    def test_get_collection_object(self):
        self.assertEqual(Apple.db_name, TEST_DB_NAME)
        self.assertIsInstance(Apple.db, Database)
        self.assertEquals(Apple.collection_name, 'apples')
        self.assertNotEqual(Apple.collection, None)
        self.assertIsInstance(Apple.collection, Collection)

        self.assertEqual(CustardApple.db_name, TEST_DB_NAME1)
        self.assertIsInstance(CustardApple.db, Database)
        self.assertEquals(CustardApple.collection_name, 'custard_apples')
        self.assertNotEqual(CustardApple.collection, None)
        self.assertIsInstance(CustardApple.collection, Collection)

        self.assertEqual(Orange.db_name, TEST_DB_NAME)
        self.assertIsInstance(Orange.db, Database)
        self.assertEquals(Orange.collection_name, 'oranges')
        self.assertNotEqual(Orange.collection, None)
        self.assertIsInstance(Orange.collection, Collection)

        self.assertEqual(WaterMelon.db_name, TEST_DB_NAME)
        self.assertIsInstance(WaterMelon.db, Database)
        self.assertEquals(WaterMelon.collection_name, 'watermellies')
        self.assertNotEqual(WaterMelon.collection, None)
        self.assertIsInstance(WaterMelon.collection, Collection)

    def test_model_instantiation(self):
        a1 = Apple(color='pink', taste='unflavorful')
        self.assertIsInstance(a1, Apple)
        self.assertEqual(a1.Schema, Apple.Schema)
        self.assertIsInstance(a1._fields, Apple.Schema)
        # logging.debug(a1._fields)
        self.assertEqual(a1._fields.color, 'pink')
        # logging.debug(type(a1._fields.color))
        self.assertEqual(a1._fields.taste, 'unflavorful')
        self.assertEqual(a1._fields.price, 0)
        self.assertEqual(a1._fields.is_poisoned, False)

        self.assertEqual(a1.color, 'pink')
        self.assertEqual(a1.taste, 'unflavorful')
        self.assertEqual(a1.price, 0)
        self.assertEqual(a1.is_poisoned, False)

    def test_set_attributes(self):
        """ prolly don't need this anymore since test_model_instantiation
        covers it """
        a1 = Apple(color='blue', taste='sweet', price=60)
        self.assertIsInstance(a1, Apple)
        self.assertEqual(a1.color, 'blue')
        self.assertEqual(a1.taste, 'sweet')
        self.assertEqual(a1.price, 60)
        # logging.debug(a1.schema)

        a1 = Apple(taste='sour')
        self.assertEqual(a1.color, 'red')
        self.assertEqual(a1.taste, 'sour')
        self.assertEqual(a1.price, 0)

    def test_insert(self):
        a1 = Apple.insert({'color': 'orange', 'taste': 'sweet', 'price': 20})
        self.assertNotEqual(a1._key, None)
        self.assertNotEqual(a1._id, None)
        a1_found = Apple(**Apple.get(a1._key))
        self.assertEqual(a1_found._key, a1._key)

        a2 = Apple.insert(Apple(color='green', taste='sour', price=30))
        self.assertNotEqual(a2._key, None)
        self.assertNotEqual(a2._id, None)
        a2_found = Apple(**Apple.get(a2._key))
        self.assertEqual(a2_found._key, a2._key)

    def test_clone(self):
        a1 = Apple.insert({'color': 'orange', 'taste': 'sweet', 'price': 20})
        self.assertNotEqual(a1._key, None)
        a2 = a1.clone()
        self.assertNotEqual(a2._key, None)
        self.assertNotEqual(a2._key, a1._key)

        a3 = Apple({'color': 'orange', 'taste': 'sweet', 'price': 20})
        self.assertRaises(AttributeError, a3.clone)

    def test_class_attributes(self):
        a1 = Apple.insert({'color': 'pink', 'taste': 'sweet', 'price': 20})
        a2 = Apple.insert({'color': 'blue', 'taste': 'sweet', 'price': 20})
        # logging.debug(Apple.collection, a1.collection, a2.collection)
        self.assertEqual(Apple.collection, a1.collection)
        self.assertEqual(Apple.collection, a2.collection)
        self.assertEqual(a1.collection, a2.collection)
        self.assertEqual(Apple.Schema, a1.Schema)
        self.assertEqual(Apple.Schema, a2.Schema)
        self.assertEqual(a1.Schema, a2.Schema)

    def test_intercepted_calls(self):
        self.assertTrue(callable(Apple.all))
        self.assertTrue(callable(Orange.all))
        self.assertTrue(callable(Apple.find))
        self.assertTrue(callable(Orange.find))
        self.assertTrue(callable(Apple.insert))
        self.assertTrue(callable(Orange.insert))

    # # def test_proxied_method_handlers(self):
    # #     logging.debug(Apple.all)
    # #     self.assertEqual(Apple.all, Apple.proxied.all)


if __name__ == '__main__':
    unittest.main()
