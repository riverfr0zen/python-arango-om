import logging
import unittest
from arango import ArangoClient
from arango.database import Database
from pao import ConnectionPool, OMException
from pao.tests.utils import resolve_integration
try:
    import settings
except:
    settings = resolve_integration()


class TestConnectionPool(unittest.TestCase):
    TEST_DB_NAME = "%s_test" % settings.DB_NAME

    def test_connecting(self):
        with self.assertRaises(OMException):
            ConnectionPool.connect(settings.DB_CLI, 'SOMENONEXISTENT_DB')
            self.assertEqual(len(ConnectionPool.dbs, 0))
        # In case something else has established a connection already
        ConnectionPool.reset()
        ConnectionPool.connect(settings.DB_CLI, self.TEST_DB_NAME)
        self.assertIsNotNone(ConnectionPool.dbcli_settings)
        self.assertIsInstance(ConnectionPool.dbcli, ArangoClient)
        self.assertEqual(ConnectionPool.default_db_name, self.TEST_DB_NAME)
        self.assertEqual(len(ConnectionPool.dbs), 1)
        self.assertIsInstance(ConnectionPool.dbs[self.TEST_DB_NAME], Database)

        with self.assertRaises(OMException):
            ConnectionPool.connect(settings.DB_CLI, self.TEST_DB_NAME)

    def test_get_db(self):
        db1_name = "%s_1" % self.TEST_DB_NAME
        ConnectionPool.reset()
        ConnectionPool.connect(settings.DB_CLI, self.TEST_DB_NAME)
        this_dbname, this_db = ConnectionPool.get_db(db1_name)
        self.assertIsNotNone(ConnectionPool.dbcli_settings)
        self.assertIsInstance(ConnectionPool.dbcli, ArangoClient)
        self.assertEqual(ConnectionPool.default_db_name, self.TEST_DB_NAME)
        self.assertEqual(len(ConnectionPool.dbs), 2)
        self.assertIsInstance(ConnectionPool.dbs[db1_name], Database)
        self.assertEqual(this_dbname, db1_name)
        self.assertIsInstance(this_db, Database)
        self.assertEqual(this_db, ConnectionPool.dbs[db1_name])

        db2_name = "%s_DOESNOTEXIST" % self.TEST_DB_NAME
        with self.assertRaises(OMException):
            ConnectionPool.get_db(db2_name)
        self.assertIsNotNone(ConnectionPool.dbcli_settings)
        self.assertIsInstance(ConnectionPool.dbcli, ArangoClient)
        self.assertEqual(ConnectionPool.default_db_name, self.TEST_DB_NAME)
        self.assertEqual(len(ConnectionPool.dbs), 2)
        assert(db2_name not in ConnectionPool.dbs)

    def test_reset(self):
        ConnectionPool.reset()
        self.assertIsNone(ConnectionPool.dbcli_settings)
        self.assertIsNone(ConnectionPool.dbcli)
        self.assertIsNone(ConnectionPool.default_db_name)
        self.assertEqual(len(ConnectionPool.dbs), 0)
