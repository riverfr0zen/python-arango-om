import unittest
import logging
import json
import os
from arango.cursor import Cursor
from schematics.types import StringType, IntType
from pao import Model, ResultSet
from pao.tests.pao_integrated_test_case import PAOIntegratedTestCase


class SMBCharacter(Model):
    class Schema(Model.Schema):
        position = IntType(required=True)
        name = StringType(required=True)
        affinity = StringType(required=True, choices=('good', 'bad',))

    def __str__(self):
        return "Super Mario Bros Character: %s" % self.name

class TestResultSet(PAOIntegratedTestCase):
    @classmethod
    def setupClass(cls):
        cls.fixtures = {}
        path = os.path.dirname(os.path.realpath(__file__))
        with open("%s/../fixtures/smb_characters.json" % path) as data:
            cls.fixtures['smb_chars'] = json.load(data)

    def setUp(self):
        super().setUp()
        for char in self.fixtures['smb_chars']:
            SMBCharacter.collection.insert(char)

    def test_init(self):
        # logging.debug(self.fixtures['smb_chars'])
        rs = ResultSet(SMBCharacter, SMBCharacter.collection.all())
        # logging.debug(rs)
        self.assertEqual(str(rs), "ResultSet of smb_characters")
        self.assertEqual(len(rs), 8)
        self.assertEqual(rs.model, SMBCharacter)
        self.assertIsInstance(rs.cursor, Cursor)

    def test_next(self):
        rs = ResultSet(model=SMBCharacter,
                       cursor=SMBCharacter.db.aql.execute(
                           "FOR c in smb_characters"
                           "  SORT c.position ASC RETURN c"
                       ))

        mario = next(rs)
        # logging.debug(mario)
        self.assertIsInstance(mario, Model)
        self.assertEqual(mario.name, 'Mario')
        self.assertEqual(mario.affinity, 'good')

        luigi = next(rs)
        self.assertIsInstance(luigi, Model)
        self.assertEqual(luigi.name, 'Luigi')
        self.assertEqual(luigi.affinity, 'good')

        next(rs)
        next(rs)
        next(rs)
        next(rs)
        next(rs)

        ludwig = next(rs)
        self.assertIsInstance(ludwig, Model)
        self.assertEqual(ludwig.name, 'Ludwig')
        self.assertEqual(ludwig.affinity, 'bad')

        with self.assertRaises(StopIteration):
            next(rs)

if __name__ == '__main__':
    unittest.main()
