import unittest
from arango import ArangoClient
from pao import ConnectionPool
from pao.tests.utils import resolve_integration
try:
    import settings
except:
    settings = resolve_integration()


TEST_DB_NAME = "pao_test"
TEST_DB_NAME1 = "pao_test_1"
ConnectionPool.reset()
ConnectionPool.connect(settings.DB_CLI,
                       TEST_DB_NAME,
                       non_integrated_mode=True)


class PAOTestCase(unittest.TestCase):
    def setUp(self):
        if len(ConnectionPool.dbs) < 1:
            ConnectionPool.connect(settings.DB_CLI,
                                   TEST_DB_NAME,
                                   non_integrated_mode=True)

    def tearDown(self):
        ConnectionPool.reset()
