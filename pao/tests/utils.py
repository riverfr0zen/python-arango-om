import logging
import os
import logging
from pao import OMException


class OMSettingsHolder(object):
    DB_CLI = {}
    DB_NAME = None


def resolve_integration():
    settings = OMSettingsHolder()
    for env_key, env_val in os.environ.items():
        if 'PAO_' in env_key:
            if 'DB_CLI' in env_key:
                cli_setting_name, cli_setting = env_key.split('__')
                if cli_setting == 'enable_logging':
                    env_val = True if env_val.lower() == 'y' else False
                settings.DB_CLI[cli_setting] = env_val
            if 'DB_NAME' in env_key:
                settings.DB_NAME = env_val
    if 'protocol' in settings.DB_CLI and settings.DB_NAME is not None:
        return settings
    raise OMException("\n\n"
                      "Integration tests require database settings. You can:\n"
                      "1) Create or add to a settings.py file at the top "
                      "level of your project. (See settings.example.py)\n"
                      "2) Initialize settings some other way\n"
                      "3) Run setup_pao_env.py to store settings in ENV")
