import logging
from inflection import pluralize, underscore
from arango import ArangoClient
from arango.database import Database
from arango.exceptions import CollectionListError
from schematics.models import Model as SchematicsModel
from schematics.types import StringType

MODEL_REGISTRY = {}
CLASS_IDX = 0
COLL_IDX = 1
COUNT_IDX = 2


class OMException(Exception):
    pass


class ConnectionPool:
    non_integrated_mode = False
    non_integrated_dbs = []

    dbcli_settings = None
    dbcli = None
    default_db_name = None
    dbs = {}

    @classmethod
    def reset(cls):
        cls.dbcli_settings = None
        cls.dbcli = None
        cls.default_db_name = None
        cls.dbs = {}
        cls.non_integrated_mode = False

    @classmethod
    def _get_db_non_integrated(cls, name=None):
        if name is None:
            name = cls.default_db_name
        if name not in cls.non_integrated_dbs:
            raise(OMException("Could not get database '%s'" % name))
        else:
            return name, Database()

    @classmethod
    def get_db(cls, name=None):
        if cls.non_integrated_mode is True:
            return cls._get_db_non_integrated(name)

        if not isinstance(cls.dbcli, ArangoClient):
            if cls.dbcli_settings is not None:
                cls.connect(cls.dbcli_settings, cls.default_db_name)
            else:
                raise OMException("No dbcli_settings configured "
                                  "for db client.")

        if name is None:
            name = cls.default_db_name
        if name not in cls.dbs or not isinstance(cls.dbs[name], Database):
            cls.dbs[name] = cls.dbcli.database(name)

        # Surface connection issues such as auth. early
        try:
            cls.dbs[name].collections()
        except CollectionListError as coll_err:
            cls.dbs.pop(name)
            raise(OMException("Could not get database '%s': %s"
                              % (name, coll_err)))
        return name, cls.dbs[name]

    @classmethod
    def _connect_non_integrated(cls):
        if cls.default_db_name not in cls.non_integrated_dbs:
            raise(OMException("Could not get default database '%s'"
                              % cls.default_db_name))

    @classmethod
    def connect(cls, dbcli_settings, default_db_name,
                non_integrated_mode=False, non_integrated_dbs=[]):
        if non_integrated_mode is True:
            cls.non_integrated_mode = True
            cls.default_db_name = default_db_name
            return

        if (cls.default_db_name is not None and
           cls.default_db_name in cls.dbs and
           isinstance(cls.dbs[cls.default_db_name], Database)):
            raise OMException("Already connected to db '%s'. Reset before "
                              "connecting again.")

        cls.dbcli_settings = dbcli_settings
        cls.default_db_name = default_db_name
        cls.dbcli = ArangoClient(**cls.dbcli_settings)
        cls.dbs[cls.default_db_name] = cls.dbcli.database(cls.default_db_name)
        # Surface connection issues such as auth. early
        try:
            cls.dbs[cls.default_db_name].collections()
        except CollectionListError as coll_err:
            cls.dbs.pop(cls.default_db_name)
            raise(OMException("Could not get default database '%s': %s"
                              % (cls.default_db_name, coll_err)))


class InterceptingProxyMeta(type):
    """
    By setting this as a metaclass, and assigning any object to
    the 'proxied' property of your class, you can call any attributes on the
    'proxied' object directly as though they were attributes of your class.
    Note, this only works for classes, not instances of your classes.

    You can also specify 'proxy_method_handlers' in your class to further
    control how the proxied methods are invoked. For details on this,
    see tests/test_class_method_interceptor.py
    """
    proxied = None
    proxy_method_handlers = {}

    def __getattr__(mcs, name):
        try:
            attr = getattr(mcs.proxied, name)
            if callable(attr):
                def proxy(*args, **kwargs):
                    handler = None
                    ret = None
                    for hname, mnames in mcs.proxy_method_handlers.items():
                        if name in mnames:
                            handler = getattr(mcs, hname)
                            break
                    if handler is not None:
                        ret = handler(attr, *args, **kwargs)
                    else:
                        ret = attr(*args, **kwargs)
                    return ret
                return proxy
            else:
                return attr
        except AttributeError:
            e = "Neither '%s' nor proxied '%s' object have attribute '%s'" \
                % (mcs.__name__, mcs.proxied.__class__, name)
            raise AttributeError(e)


class ModelMeta(InterceptingProxyMeta):
    @classmethod
    def __prepare__(mcs, name, bases, is_abstract=False, **kwargs):
        return super().__prepare__(name, bases)

    def __new__(mcs, name, bases, attrs, is_abstract=False, **kwargs):
        return super().__new__(mcs, name, bases, attrs)

    def __init__(cls, name, bases, attrs, is_abstract=False, **kwargs):
        super().__init__(name, bases, attrs)

        if is_abstract is False:
            if 'db_name' in kwargs:
                cls.db_name = kwargs['db_name']
            else:
                cls.db_name = ConnectionPool.default_db_name
            if 'collection_name' in kwargs:
                cls.collection_name = kwargs['collection_name']
            else:
                cls.collection_name = underscore(pluralize(name)).lower()
            if hasattr(cls, 'graph_name'):
                if 'graph_name' in kwargs:
                    cls.graph_name = kwargs['graph_name']
                else:
                    cls.graph_name = 'default'

            cls._initialize_model()

            if len(bases) > 1:
                schema_bases = tuple(sbase.Schema for sbase in bases
                                     if issubclass(sbase.Schema,
                                                   SchematicsModel))
                cls.Schema = type(name + 'Schema',
                                  schema_bases,
                                  cls.new_schema_attrs)


class Model(metaclass=ModelMeta, is_abstract=True):
    db_name = None
    db = None
    collection_name = None
    collection = None

    class Schema(SchematicsModel):
        _id = StringType()
        _key = StringType()
        _rev = StringType()

    def __init__(self, *args, **kwargs):
        self._set_doc_properties(**kwargs)

    def _set_doc_properties(self, **kwargs):
        self.sync = kwargs.pop('sync', None)
        self._fields = self.Schema(kwargs)
        for field_name in self._fields:
            setattr(self, field_name, getattr(self._fields, field_name))

    @classmethod
    def _initialize_model(cls):
        if not ConnectionPool.non_integrated_mode:
            cls.db_name, cls.db = ConnectionPool.get_db(cls.db_name)
            cls._initialize_db_objects()
        # @TODO: Implement with updating after creates/updates!
        # count = cls.collection.count()
        MODEL_REGISTRY.update({cls.__name__: (cls,
                                              cls.collection_name,
                                              # @TODO
                                              # count),
                                              )})

    @classmethod
    def _initialize_db_objects(cls):
        cls.collection = cls._get_collection_object(force=True)
        cls.proxied = cls.collection

    @classmethod
    def _get_collection_object(cls, force=False):
        if cls.collection is None or force is True:
            collections = [collection for collection in cls.db.collections()
                           if collection['name'] == cls.collection_name]
            if len(collections) > 0:
                return cls.db.collection(cls.collection_name)
            else:
                return cls.db.create_collection(cls.collection_name)

    @classmethod
    def insert(cls, inputObj):
        if isinstance(inputObj, Model):
            inputObj = inputObj._fields.to_primitive()
            inputObj.pop('_key', None)
            inputObj.pop('_id', None)
            inputObj.pop('_rev', None)
        result = cls.collection.insert(inputObj)
        inputObj.update(result)
        return cls(**inputObj)

    def clone(self):
        """ Implementation only works because of how insert() is
        implemented. May need to change in the future """
        if getattr(self, '_id', None) is None:
            raise AttributeError("Cannot clone unsaved object")
        return self.insert(self)


class GraphMemberModel(Model, is_abstract=True):
    graph_name = None
    graph = None

    @classmethod
    def _initialize_db_objects(cls):
        cls.graph = cls._get_graph_object(force=True)
        super(GraphMemberModel, cls)._initialize_db_objects()

    @classmethod
    def _get_graph_object(cls, force=False):
        if (cls.graph is None or force is True):
            graphs = [graph for graph in cls.db.graphs()
                      if graph['name'] == cls.graph_name]
            if len(graphs) > 0:
                return cls.db.graph(cls.graph_name)
            else:
                return cls.db.create_graph(cls.graph_name)


class VertexModel(GraphMemberModel, is_abstract=True):
    @classmethod
    def _get_collection_object(cls, force=False):
        if cls.collection is None or force is True:
            collections = [collection for collection
                           in cls.graph.vertex_collections()
                           if collection == cls.collection_name]
            if len(collections) > 0:
                return cls.graph.vertex_collection(cls.collection_name)
            else:
                return cls.graph.create_vertex_collection(cls.collection_name)

    def create_path_to(self, destination, edge_class, properties={}):
        # @TODO implement
        properties.update({"_from": self._id, "_to": destination._id})
        return edge_class.insert(properties)

    def traverse(self, **kwargs):
        return self.graph.traverse(self._id, **kwargs)

    @property
    def to_edges(self):
        return [classname for classname, regdata
                in MODEL_REGISTRY.items()
                if issubclass(regdata[CLASS_IDX], EdgeModel) and
                self.__class__.__name__ in regdata[CLASS_IDX].from_models]

    @property
    def from_edges(self):
        return [classname for classname, regdata
                in MODEL_REGISTRY.items()
                if issubclass(regdata[CLASS_IDX], EdgeModel) and
                self.__class__.__name__ in regdata[CLASS_IDX].to_models]


class EdgeModel(GraphMemberModel, is_abstract=True):
    from_models = []
    to_models = []

    class Schema(GraphMemberModel.Schema):
        _from = StringType(required=True)
        _to = StringType(required=True)

    @classmethod
    def from_collections(cls):
        return [MODEL_REGISTRY[modelname][COLL_IDX] for modelname
                in cls.from_models
                if modelname in MODEL_REGISTRY]

    @classmethod
    def to_collections(cls):
        return [MODEL_REGISTRY[modelname][COLL_IDX] for modelname
                in cls.to_models
                if modelname in MODEL_REGISTRY]

    @classmethod
    def _get_collection_object(cls, force=False):
        if cls.collection is None or force is True:
            # logging.debug(cls.graph.edge_definitions())
            collections = [collection for collection
                           in cls.graph.edge_definitions()
                           if collection['name'] == cls.collection_name]
            if len(collections) > 0:
                return cls.graph.edge_collection(cls.collection_name)
            else:
                # logging.debug(cls.from_collections())
                # logging.debug(cls.to_collections())
                return cls.graph.create_edge_definition(
                    name=cls.collection_name,
                    from_collections=cls.from_collections(),
                    to_collections=cls.to_collections()
                )


class ResultSet(object):
    model = None
    cursor = None

    def __str__(self):
        return "ResultSet of %s" % self.model.collection_name

    def __init__(self, model=None, cursor=None):
        self.cursor = cursor
        self.model = model

    def __len__(self):
        return self.cursor.count()

    def __iter__(self):
        return self

    def __next__(self):
        return self.model(**next(self.cursor))

    def count(self):
        return self.cursor.count()
