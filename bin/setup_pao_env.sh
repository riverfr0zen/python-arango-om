#!/bin/bash

#
# @TODO update this
#

echo "Enter DB client protocol (e.g. http):"
read PAO_DB_CLI__protocol
export PAO_DB_CLI__protocol=${PAO_DB_CLI__protocol}


echo "Enter DB host (e.g. localhost):"
read PAO_DB_CLI__host
export PAO_DB_CLI__host=${PAO_DB_CLI__host}

echo "Enter DB host port (e.g. 8529):"
read PAO_DB_CLI__port
export PAO_DB_CLI__port=${PAO_DB_CLI__port}


echo "Enter the base DB name (e.g. myprojectdb):"
read PAO_DB_NAME
export PAO_DB_NAME=${PAO_DB_NAME}


echo "Enter DB username (e.g. myuser):"
read PAO_DB_CLI__username
export PAO_DB_CLI__username=${PAO_DB_CLI__username}


echo "Enter DB password (e.g. mypassword):"
echo "WARNING: Password will be printed to screen at end of script!"
read PAO_DB_CLI__password
export PAO_DB_CLI__password=${PAO_DB_CLI__password}


echo "Enable DB logging? (y/n):"
read PAO_DB_CLI__enable_logging
export PAO_DB_CLI__enable_logging=${PAO_DB_CLI__enable_logging}

printf "\n\nSettings have been added to env for this session. "
printf "If you want to reuse them, you can copy the lines below "
printf "to a bash script and run it next time.\n\n"
printf "#!/bin/bash\n"
printf "export PAO_DB_CLI__enable_logging=${PAO_DB_CLI__protocol}\n"
printf "export PAO_DB_CLI__host=${PAO_DB_CLI__host}\n"
printf "export PAO_DB_CLI__port=${PAO_DB_CLI__port}\n"
printf "export PAO_DB_NAME=${PAO_DB_NAME}\n"
printf "export PAO_DB_CLI__username=${PAO_DB_CLI__username}\n"
printf "export PAO_DB_CLI__password=${PAO_DB_CLI__password}\n"
printf "export PAO_DB_CLI__enable_logging=${PAO_DB_CLI__enable_logging}\n"
printf "\n\n"
