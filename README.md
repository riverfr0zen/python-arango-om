# python-arango-om

Object mapping layer for ArangoDB built on top of the python-arango driver. This project is in very early stages of development. Please see contents of `pao/tests/` if curious. Further documentation will follow as we approach release 0.1.0.

