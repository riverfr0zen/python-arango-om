import logging

#
# Database
#
DB_CLI = {
    'protocol': 'http',
    'host': 'localhost',
    'port': 8529,
    'username': 'my_username',
    'password': 'my_password',
    'enable_logging': True
}

DB_NAME = 'pao'


#
# Logging
#
LOG_LEVELS = {
    'root': logging.DEBUG,
    'arango': logging.WARNING,
    'requests': logging.ERROR
}
logging.basicConfig(level=LOG_LEVELS['root'])
logging.getLogger("arango").setLevel(LOG_LEVELS['arango'])
logging.getLogger("requests").setLevel(LOG_LEVELS['requests'])
logging.getLogger("urllib3").setLevel(LOG_LEVELS['requests'])
